Feature: Create subtasks
	As a ToDo App user
	I should be able to create a subtask
	So I can break down my tasks in smaller pieces

Background:
	Given I visit the system page
	And I click on My Tasks
	Then I have to go to My Tasks page
	And I click on a task
	And I have to see the task ID
	And I have to see the task description


Scenario: Create a new subtask
	Given I click on Manage Subtasks
	When I insert the subtask description
	And I insert the due date
	And I click on add button
	Then I must have a new subtask
	And I have to see the subtask below the main task

Scenario: Subtask without description
	Given I click on Manage Subtasks
	When I insert the due date
	And I click on add button
	Then I should receive a error message about no description

Scenario: Subtask without due date
	Given I click on Manage Subtasks
	When I insert the subtask description
	And I click on add button
	Then I should receive an error message about no due date

