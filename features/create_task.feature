Feature: Create Task
	As a ToDo App user
	I should be able to create a task
	So I can manage my tasks


Background:
	Given I visit the system page
	And I click on My Tasks
	Then I have to go to My Tasks page

Scenario: Create a new task clicking on add button
	Given I click on Add Task button
	When I see the new task page
	And I insert the task tittle
	And I click on save button
	Then I have to receive a success message
	And I have to see the task on the list

Scenario: Create a new task hitting enter on keyboard
	Given I hit enter on keyboard
	When I see the new task page
	And I insert the task tittle
	And I click on save button
	Then I have to receive a success message
	And I have to see the task on the list

Scenario Outline: Error creating a task
	Given I click on Add Task button
	When I see the new task page
	And I insert the wrong number of "<characters>"  
	And I click on save button
	Then I have to receive an error message

	Examples:
	| characters |
	| ab        |
	| testetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetestetesteteste |
