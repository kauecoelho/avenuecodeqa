Given(/^I visit the system page$/) do
  visit "/"
end

Given(/^I click on My Tasks$/) do
  page.find('#mytaskbutton').click
end

Then(/^I have to go to My Tasks page$/) do
  expect(page).to have_selector('#somethingFromMyTaskPage', visible: true)
end

Given(/^I click on Add Task button$/) do
  page.find('#addButton').click
end

When(/^I see the new task page$/) do
  expect(page).to have_selector('#somethingFromNewTaskPage', visible: true)
end

When(/^I insert the task tittle$/) do
  fill_in 'myTaskTitle', :with => 'some text'
end

When(/^I click on save button$/) do
  page.find('#saveButton').click
end

Then(/^I have to receive a success message$/) do
  page.should have_content "Task created"
end

Then(/^I have to see the task on the list$/) do
  page.find("taskId")
end

Given(/^I hit enter on keyboard$/) do
  driver.action.send_keys(elementVisible, :tab).send_keys(elementVisible, :return).perform
#I don't know if this is correct, never tried this code. Found this on internet and hope that this is a correct method to hit enter on keyboard
end

When(/^I insert the wrong number of \"([^\"]*)\"$"/) do |characters|
  fill_in 'myTaskTitle', :with => characters
end

Then(/^I have to receive an error message$/) do
  page.should have_content "Task not created, title must have 3 to 250 characters"
end