Given(/^I visit the system page$/) do
  visit "/"
end

Given(/^I click on My Tasks$/) do
  page.find('#mytaskbutton').click
end

Then(/^I have to go to My Tasks page$/) do
  expect(page).to have_selector('#somethingFromMyTaskPage', visible: true)
end

Then(/^I click on a task$/) do
  page.find('#task').click
end

Then(/^I have to see the task ID$/) do
  expect(page).to have_content('#taskId')
end

Then(/^I have to see the task description$/) do
  expect(page).to have_content('#taskDescription')
end

Given(/^I click on Manage Subtasks$/) do
  page.find('#manageSubtasks').click
end

When(/^I insert the subtask description$/) do
  fill_in 'subtask_description', :with => 'some text'
end

When(/^I insert the due date$/) do
  fill_in 'subtask_due_date', :with => '01/01/2017'
end

When(/^I click on add button$/) do
  page.find('#add_subtask').click
end

Then(/^I must have a new subtask$/) do
  page.should have_content "SubTask created"
end

Then(/^I have to see the subtask below the main task$/) do
  expect(page).to have_content('#subtaskId', visible: true)
end

Then(/^I should receive a error message about no description$/) do
  page.should have_content "Task not created, there is no description"
end

Then(/^I should receive an error message about no due date$/) do
  page.should have_content "Task not created, there is no due date"
end